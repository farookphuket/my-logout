
# my-logout

> **my-logout** is my first Haskell script that I just copy code from [Building a GTK App With Haskell (In Under An Hour)](https://www.youtube.com/watch?v=ViW-bcNQ6Lc) by [DT](https://www.youtube.com/c/DistroTube) on 28 aug 2022 

> the video url is

> `https://www.youtube.com/watch?v=ViW-bcNQ6Lc`

> I use it for the replacement of "**[i3exit](https://gist.github.com/beast2013/4fa8f8d12adace070afc)**"

[my_logout]:https://archive.org/download/arch_linux_myconfig_24-aug-2022_edit/my-logout_1.0.png




## my-logout 1.0


![my-logout][my_logout]


> ***my-logout*** is the Haskell script that will be find in `~/.config/dwm/my-logout` as a path of your [DWM](https://github.com/farookphuket/simple_arch) script that will automatically clone into your computer since you have run :

> `cd ~/ && git clone https://github.com/farookphuket/simple_arch.git && cd ~/simple_arch && sh setup.sh`


## try this script alone

> to try this script without my [simple_arch](https://github.com/farookphuket/simple_arch) you need to install stack on your linux by issue the command
> `curl -sSL https://get.haskellstack.org/ | sh`

> or go to [READ DOCS](https://docs.haskellstack.org/en/stable/README/) for more info

> for better understand this please watch the youtube video `https://www.youtube.com/watch?v=ViW-bcNQ6Lc` here.

## ===== last update 28 Aug 2022
> just the first script that I learn to create the program on Arch Linux 
> so now I am happy with my logout screen
